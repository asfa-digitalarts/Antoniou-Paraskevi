/*
 * @name Video Canvas
 * @description <p>Load a video with multiple formats and draw it to the canvas.</p>
 * <p><em><span class="small"> To run this example locally, you will need the
 * <a href="http://p5js.org/reference/#/libraries/p5.dom">p5.dom library</a>
 * at least one video file, and a running <a href="https://github.com/processing/p5.js/wiki/Local-server">local server</a>.</span></em></p>
 */
 var screen = 1;
var numberOfPages=4;

 var radius = 10;
    var amount = 5;
    var spacing = 360/amount;
let fingers;
let fingers1;
let fingers2;
let fingers3;

function setup() {
  createCanvas(1280, 720);
  // specify multiple formats for different browsers
  fingers = createVideo(['data/16.mp4', 'assets/fingers.webm']);
  fingers1= createVideo(['data/15.mp4']);
  fingers2= createVideo (['data/17.mp4']);
  fingers3= createVideo (['data/18.mp4']);
  
  fingers.hide(); // by default video shows up in separate dom
  // element. hide it and draw it to the canvas
  // instead
  
  
  
  
}

function draw() {
  background(66,135,245);
   push();
    translate(600, 300);

    for (var i = 0; i < amount; i++) {
        push();
        rotate(i*spacing);
        var num = new Num(89, 0 + radius, 0, 90, 255);
        num.render();
        pop();
    }
    pop();
}

function Num(msg, x, y, rot, clr) {
    this.x = x;
    this.y = y;
    this.rot = rot;
    this.msg = msg;
    this.color = clr;

    this.render = function() {
        push();
        fill(this.color);
        translate(this.x, this.y);
        rotate(this.rot);
        text(this.msg, 0, 0);
        pop();
    }

  
  
  
  
  
  if (screen==1) {
  image(fingers, 0, 0,600,400); 
  }// draw the video frame to canvas
  //filter(GRAY);
  else if (screen==2){
  image(fingers1, 0, 0,600,400); // draw a second copy to canvas
}
else if (screen==3){
  image(fingers2, 0,0,600,400);
}
else if (screen==4){
  image (fingers3,0,0);
}
}

function mousePressed() {
   screen++;
   if (screen>numberOfPages){
      screen=1;
  fingers.play(); // set the video to loop and start playing
}
if (screen==2){
  fingers.stop ();
fingers1.play();

}else if (screen==3){
  fingers1.stop ();
  fingers2.play ();
}
else if (screen==4) {
  fingers2.stop ();
  fingers3.stop();
}
}
