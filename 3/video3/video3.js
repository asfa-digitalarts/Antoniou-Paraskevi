/*
 * @name Video Canvas
 * @description <p>Load a video with multiple formats and draw it to the canvas.</p>
 * <p><em><span class="small"> To run this example locally, you will need the
 * <a href="http://p5js.org/reference/#/libraries/p5.dom">p5.dom library</a>
 * at least one video file, and a running <a href="https://github.com/processing/p5.js/wiki/Local-server">local server</a>.</span></em></p>
 */
 var screen = 1;
var numberOfPages=8;
let fingers;
let fingers1;
let fingers2;
let fingers3;
let fingers4;
let fingers5;
let fingers6;
let fingers7;


function setup() {
  createCanvas(1280, 720);
  // specify multiple formats for different browsers
  fingers = createVideo(['data/fingers.mp4', 'assets/fingers.webm']);
  fingers1= createVideo(['data/fingers1.mp4']);
  fingers2= createVideo (['data/fingers2.mp4']);
  fingers3= createVideo (['data/fingers3.mp4']);
  fingers4= createVideo (['data/fingers4.mp4']);
  fingers5= createVideo (['data/fingers5.mp4']);
  fingers6= createVideo (['data/fingers6.mp4']);
  fingers7= createVideo (['data/18.mp4']);
  
  fingers.hide(); // by default video shows up in separate dom
  // element. hide it and draw it to the canvas
  // instead
}

function draw() {
  background(66,135,245);
  if (screen==1) {
  image(fingers, 0, 0); 
  }// draw the video frame to canvas
  //filter(GRAY);
  else if (screen==2){
  image(fingers1, 0, 0); // draw a second copy to canvas
}
else if (screen==3){
  image(fingers2, 0,0);
}
else if (screen==4){
  image (fingers3,0,0);
}
else if (screen==5){
  image (fingers4,0,0);
}
else if (screen==6) {
  image (fingers5,0,0);
}
else if (screen==7) {
  image (fingers6,0,0) ;
} 
else if (screen==8) {
  image (fingers7,0,0) ;
}
}

function mousePressed() {
   screen++;
   if (screen>numberOfPages){
      screen=1;
  fingers.play(); // set the video to loop and start playing
}
if (screen==2){
  fingers.stop ();
fingers1.play();

}else if (screen==3){
  fingers1.stop ();
  fingers2.play ();
}
else if (screen==4) {
  fingers2.stop ();
  fingers3.play ();
}
else if (screen==5) {
  fingers3.stop ();
  fingers4.play ();
}
else if (screen==6) {
  fingers4.stop ();
  fingers5.play (); 
}
else if (screen==7) {
  fingers5.stop ();
  fingers6.play ();
}

else if (screen==8) {
  fingers6.stop ();
  fingers7.stop ();
}
}
