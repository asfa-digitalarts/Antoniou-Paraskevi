/**
 *  @name soundFormats
 *  @description <p>Technically, due to patent issues, there is no single
 *  sound format that is supported by all web browsers. While
 *  <a href="http://caniuse.com/#feat=mp3">mp3 is supported</a> across the
 *  latest versions of major browsers on OS X and Windows, for example,
 *  it may not be available on some less mainstream operating systems and
 *  browsers.</p>
 *
 *  <p>To ensure full compatibility, you can include the same sound file
 *  in multiple formats, e.g. 'sound.mp3' and 'sound.ogg'. (Ogg is an
 *  open source alternative to mp3.) You can convert audio files
 *  into web friendly formats for free online at <a href="
 *  http://media.io/">media.io</a></p>.
 *
 *  <p>The soundFormats() method tells loadSound which formats
 *  we have included with our sketch. Then, loadSound will
 *  attempt to load the first format that is supported by the
 *  client's web browser.</p>
 *
 * <p><em><span class="small"> To run this example locally, you will need the
 * <a href="http://p5js.org/reference/#/libraries/p5.sound">p5.sound library</a>
 * a sound file, and a running <a href="https://github.com/processing/p5.js/wiki/Local-server">local server</a>.</span></em></p>

 
 
 
 
 
 */
 
  let img; // Declare variable 'img'.
  let img2; // stores 2nd image
  let img3;
 
let song;
let song2;
let song3;

let ball1 = {};

let soundFile;
let playing = false;

let playing1 = false;
let fingers;
let button1;

function preload() {

  soundFormats('ogg', 'mp3');

  // if mp3 is not supported by this browser,
  // loadSound will load the ogg file
  // we have included with our sketch
  
      img = loadImage('data/ale2.jpg'); // Load the image
 img3= loadImage('data/ale1.jpg');
 song = loadSound('data/lucky_dragons_-_power_melody.mp3');
  song2 = loadSound('data/ale2.mp3');
  song3 = loadSound('data/ale1.mp3');
  
  
}

function setup() {
  createCanvas(1600, 1600);
   background(66, 135, 245);
   
    textAlign(CENTER, CENTER);
    rectMode(CENTER);
   
   image (img3,0,0,700,600);
   image (img,700,600,700,600);
     fingers = createVideo(['data/ale2.mp3', ]);
  
  button1 = createButton('play');
  
  button1.position (500,700);
     
   button1.size(100,100);
   
 

  //song.loop(); // song is ready to play during setup() because it was loaded during preload
  //song.amp(0.2);

  //button1.mousePressed1(toggleVid); // attach button listener

   button1.mousePressed(toggleVid); // attach button listener
 

 
  
  
  
}
 
function draw() {

 ball1.x = constrain(mouseX, 900, 1300);
 ball1.y= constrain(mouseY,0, height); 
 stroke(45,90,67);
    strokeWeight(2);
     fill(175);
  ellipse(ball1.x, 250, 100, 100); 
  
   push();
    translate(1100, 450);
    if (frameCount < 200) {
        scale(5 + frameCount/400);
    } else {
        scale(5);
    }
    strokeWeight(1);
    stroke(255);
    fill(0, 0);
    rect(0, 0, 50, 20);
    
   fill(255);
    noStroke();
    textFont('Arial');
    text('1989', 0, 0);
    pop();

   push();
    translate(470, 970);
    if (frameCount < 200) {
        scale(5 + frameCount/400);
    } else {
        scale(5);
    }
    strokeWeight(1);
    stroke(255);
    fill(0, 0);
    rect(0, 0, 50, 20);
    
   fill(255);
    noStroke();
    textFont('Arial');
    text('2021', 0, 0);
    pop();



}
function mousePressed() {
 
//'map the balls x location to a panning degree' ;
  // 'between (500,500,500,500)';-1.0 (left) and 1.0 (right)';

 

   
  let panning = map(ball1.x, 0, width, -1.0, 5.0);
    

 if (mouseX < 1300 && mouseX > 900) {
  if (mouseY < 300 && mouseY > 150) { 
  if //(mouseX > 500) && (mouseX < 600) && (mouseY > 500) && (mouseY < 600);
 (song3.isPlaying()) {
    // .isPlaying() returns a boolean
    song3.pause();
    } else {
  song3.play();
  //song3.stop();
    playing = !playing;
}

}
 }
 
}
function toggleVid() {
  if (playing1) {
    fingers.pause();
    button1.html('play');
 
  } else {
    fingers.loop();
    button1.html('pause');
    
  }
  playing1 = !playing1;
}
