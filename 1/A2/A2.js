/*
 * @name Load and Display Image
 * @description Images can be loaded and displayed to the screen at their
 * actual size or any other size.
 * <p><em><span class="small"> To run this example locally, you will need an 
 * image file, and a running <a href="https://github.com/processing/p5.js/wiki/Local-server">
 * local server</a>.</span></em></p>

 */
let MENU = 0;
let e1 ;
let e2;
let maskBrush;

// Declare variable 'img'.
function preload (){
 e1 = loadImage('data/e1.jpg'); 
 e2= loadImage('data/e2.jpg'); 
 img = loadImage('data/1.jpg');
 img1= loadImage('data/2.jpg'); 
 img2= loadImage('data/3.jpg'); 
 maskBrush= loadImage('data/e4.png'); 
}

function setup() {
  createCanvas(1600, 1600);
  
  
  background (66,135,245);
  
  mask= createGraphics(800,600) ;
  mask.imageMode(CENTER); 
  
  maskee1= e1.get();
  maskee1.mask(mask.get());
  

  image(e2,0,0,800,600);
  
  
}

function draw() {{
  print(mouseX, mouseY);
    image(maskee1,0,0,800,600);
    fill(237,154, 196);
    rect(1100, 50, 200, 75);
    fill(106,185, 40);
    rect(1100, 200, 200, 75);
    fill(227, 34, 27);
    rect(1100, 350, 200, 75);
    fill(255);
    text('Ήχος', 1148, 100);
    text('video', 1150, 395);
    text('tv-internet', 1110, 250);
 textSize(40);
  
  
 
 
 
  
    
    
    if (mouseIsPressed){
      mask.image(maskBrush,mouseX,mouseY,100,100);
      
      maskee1=e1.get();
      maskee1.mask(mask.get());
           
  //image (img2,10,10,500,500);
  //text("X: " +mouseX, 10, 20) ;
  //text("Y: " +mouseY, 10,40) ;
}
}

if (MENU == 1) {
  createCanvas(1600, 1600);
    background(66,135,245);
    image(img,10,10,800,600);
   
    if (mouseButton == RIGHT) {
      MENU = 0;
    }
  }
  if (MENU == 2) {
    createCanvas(1600, 1600);
    background(66,135,245);
   image(img1,0,0,800,600);
    if (mouseButton == RIGHT) {
      MENU = 0;
    }
 
  
  } // INSTRUCTIONS
  if (MENU == 3) {createCanvas(1600, 1600);
    background(66,135,245);
    image (img2,0,0,800,600);
   
     if (mouseButton == RIGHT) {
      MENU = 0;
    }
    
  }


}

  
  function mouseClicked() {
  if (MENU == 0) {
    if (mouseX < 1300 && mouseX > 1100) {
      if (mouseY < 125 && mouseY > 50) {
        MENU = 1;
      }
      if (mouseY < 275 && mouseY > 200) {
        MENU = 2;
      }
      if (mouseY < 425 && mouseY > 350) {
       MENU = 3;
      }
    }
  }
}
